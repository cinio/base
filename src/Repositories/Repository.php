<?php

namespace Cinio\Base\Repositories;

use Illuminate\Database\Eloquent\Model;
use Cinio\Base\Adhoc\DynamicAttribute;
use Cinio\Base\Adhoc\Macroable;
use Cinio\Base\Repositories\Concerns\HasSoftDelete;

abstract class Repository
{
    use Macroable, DynamicAttribute, HasSoftDelete;

    /**
     * The eloquent model
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor
     * @param Container $app
     * @param Model $model
     */
    public function __construct(Model $model = null)
    {
        $this->model = $model;
    }

    /**
     *  Return the model related to this finder.
     *
     *  @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set model
     * @param Model $model
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function setModel(Model $model)
    {
        return $this->model = $model;
    }

    /**
     *  Check if the model's table exists
     *
     *  @return boolean
     */
    public function tableExists()
    {
        return $this->model->getConnection()->getSchemaBuilder()->hasTable($this->model->getTable());
    }

    /**
     * Returns total number of entries in DB.
     * @return integer
     */
    public function count()
    {
        return $this->model->count();
    }
}
