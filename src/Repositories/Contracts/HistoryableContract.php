<?php

namespace Cinio\Base\Repositories\Contracts;

use Illuminate\Support\Collection;
use Cinio\Base\Models\Contracts\Stateable;

interface HistoryableContract
{
    /**
     * Move state of a certain entity
     * @param Stateable $moveable
     * @param Stateable $state
     * @param array $attributes
     */
    public function changeHistory(Stateable $moveable, Stateable $state, array $attributes = []);

    /**
     * Move state of a certain entity
     * @param Collection $moveable
     * @param Stateable $state
     * @param array $attributes
     */
    public function bulkChangeHistory(Collection $moveables, Stateable $state, array $attributes = []);
}
