<?php

namespace Cinio\Base\Repositories\Contracts;

interface SoftDeleteable
{
    /**
     * Retrieve all trashed.
     * @param array $related
     * @param number $perPage
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function trashed($related = [], $perPage = 0, $select = ['*']);

    /**
     * Retrieve a single record by id.
     * @param unknown $id
     * @param array $related
     * @param array $select
     * @return Illuminate\Database\Eloquent\Model
     */
    public function findTrashed($id, $related = [], $select = ['*']);

    /**
     * Restore a record.
     * @param unknown $id
     * @return Illuminate\Database\Eloquent\Model
     */
    public function restore($id);
}
