<?php

namespace Cinio\Base\Repositories\Concerns;

trait HasSlug
{
    /**
     * The slug column
     * @var string
     */
    protected $slug = 'name';

    /**
     * The transaction model
     *
     * @var Model
     */
    protected $transactionModel;

    /**
     * Find a record by slug
     * @param unknown $slug
     * @param array $with
     * @param array $select
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findBySlug($slug, array $with = [], $select = ['*'])
    {
        return $this->model->where($this->slug, $slug)->with($with)->first($select);
    }

    /**
     * Find a record by slug
     * @param unknown $slug
     * @param array $with
     * @param array $select
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findBySlugOrFail($slug, array $with = [], $select = ['*'])
    {
        return $this->model->where($this->slug, $slug)->with($with)->firstOrFail($select);
    }

    /**
     * Edit a record by slug
     * @param unknown $slug
     * @param array $attributes
     * @param boolean $forceFill
     * @return boolean
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function editBySlug($slug, array $attributes = [], $forceFill = false)
    {
        $model = $this->model->where($this->slug, $slug)->firstOrFail();

        if ($forceFill) {
            if ($model->fill($attributes)->save()) {
                return $model;
            }
        } else {
            if ($model->forceFill($attributes)->save()) {
                return $model;
            }
        }
        return false;
    }

    /**
     * Delete a record by slug
     * @param unknown $slug
     * @return integer
     */
    public function deleteBySlug($slug)
    {
        return $this->model->where($this->slug, $slug)->delete();
    }
}
