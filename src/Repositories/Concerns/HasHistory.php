<?php

namespace Cinio\Base\Repositories\Concerns;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Cinio\Base\Models\Contracts\Stateable;

trait HasHistory
{
    /**
     * The history model
     * @var unknown
     */
    protected $historyModel;

    /**
     * The current column
     * @var unknown
     */
    protected $currentColumn = 'is_current';

    /**
     * Move state of a certain entity
     * @param Stateable $moveable
     * @param Stateable $state
     * @param array $attributes
     */
    public function changeHistory(Stateable $moveable, Stateable $state, array $attributes = [])
    {
        if (isset($attributes['user_id'])) {
            throw  new \Exception('cannot overide user id');
        }

        $this->historyModel
            ->where($moveable->getForeignKey(), $moveable->getEntityId())
            ->update([$this->currentColumn => 0]);

        $attributes = array_merge($attributes, [
            $moveable->getForeignKey() => $moveable->getEntityId(),
            $state->getForeignKey()    => $state->getEntityId(),
            $this->currentColumn       => 1
        ]);

        //Update main table, should not have user_id
        $moveable->{$state->getForeignKey()} = $state->getEntityId();
        $moveable->fill($attributes);
        $moveable->save();

        return $this->historyModel
            ->newInstance($attributes)
            ->save();
    }

    /**
     * Move state of a certain entity
     * @param Collection $moveable
     * @param Stateable $state
     * @param array $attributes
     */
    public function bulkChangeHistory(Collection $moveables, Stateable $state, array $attributes = [])
    {
        if (isset($attributes['user_id'])) {
            throw  new \Exception('cannot overide user id');
        }

        if ($moveables->count() > 0) {
            $foreignKey    = $moveables->first()->getForeignKey();
            $entityKey     = $moveables->first()->getEntityKey();
            $moveableModel = $moveables->first()->getModel();

            foreach ($moveables->chunk(100) as $moveable) {
                $histories = [];
                $entityIds = $moveable->pluck($entityKey);

                $this->historyModel
                    ->whereIn($foreignKey, $entityIds)
                    ->update([$this->currentColumn => 0]);

                foreach ($entityIds as $entityId) {
                    $histories[] = array_merge($attributes, [
                        $foreignKey             => $entityId,
                        $state->getForeignKey() => $state->getEntityId(),
                        $this->currentColumn    => 1,
                        'created_at'            => Carbon::now(),
                        'updated_at'            => Carbon::now(),
                    ]);
                }

                $this->historyModel->insert($histories);

                $moveableModel
                    ->whereIn($entityKey, $entityIds)
                    ->update(array_intersect_key($attributes, $moveableModel->getFillable()));
            }
        }
    }
}
