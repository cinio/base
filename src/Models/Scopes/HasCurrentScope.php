<?php

namespace Cinio\Base\Models\Scopes;

trait HasCurrentScope
{
    /**
     * The active column
     * @var string
     */
    protected $currentColumn = 'is_current';

    /**
     * Local scope for getting current status
     * @param unknown $query
     * @param unknown $current
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where($this->currentColumn, $current);
    }
}
