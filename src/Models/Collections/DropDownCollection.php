<?php

namespace Cinio\Base\Models\Collections;

use Illuminate\Database\Eloquent\Collection;

class DropDownCollection extends Collection
{
    /**
     * Convert collection to dropdown
     *
     * @param [type] $nameField
     * @param string $idField
     * @param string $defaultNullText
     * @return array
     */
    public function dropDown($nameField, $idField = 'id', $defaultNullText = '--')
    {
        $listFieldValues = $this->pluck($nameField, $idField)->toArray();

        $selectArray     = [];
        $selectArray[''] = $defaultNullText;

        return $selectArray + $listFieldValues;
    }
}
