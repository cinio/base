<?php

namespace Cinio\Base\Models\Relations;

trait UserRelation
{
    /**
     * The user column
     * @var string
     */
    protected $userCol = 'user_id';

    /**
     * This model's relation to user
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), $this->userCol);
    }
}
