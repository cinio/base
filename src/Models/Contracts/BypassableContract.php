<?php

namespace Cinio\Base\Models\Contracts;

interface BypassableContract
{
    /**
     * Can by bypass for ownership rules
     * @return boolean
     */
    public function bypass();
}
