<?php

namespace Cinio\Base\Models\Concerns;

use Cinio\Base\Models\Collections\DropDownCollection;

trait HasDropDown
{
    /**
     * Create new collection
     *
     * @param array $models
     * @return void
     */
    public function newCollection(array $models = [])
    {
        return new DropDownCollection($models);
    }
}
