<?php

namespace Cinio\Base\Rules;

use Cinio\Base\Models\Contracts\BypassableContract;

trait CheckOwnership
{
    /**
     * The user column
     * @var string
     */
    protected $userColumn = 'user_id';

    /**
     * Check ownershup
     * @param unknown $model
     * @param unknown $user
     * @return boolean
     */
    public function isOwner($model, BypassableContract $user)
    {
        if (!$model || !$user) {
            return false;
        }

        if ($user->bypass()) {
            return true;
        }

        return $model->{$this->userColumn} == $user->id;
    }
}
