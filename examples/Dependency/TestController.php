<?php

namespace Example\Dependency;

use Example\Repository\TestContract;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cinio\Base\Paginations\DatabasePagination;
use Cinio\Base\Response\ApiResponse;
use Cinio\Base\Rules\CheckOwnership;

class TestController extends Controller
{
    use ApiResponse, CheckOwnership, DatabasePagination;

    protected $repository;

    /**
     * Constructor
     *
     * @param TestContract $test
     */
    public function __construct(TestContract $test)
    {
        // The contract must be binded properly in your service provider in order for this to work.
        $this->repository = $test;
    }

    /**
     * Result
     *
     * @return void
     */
    public function index()
    {
        $prepare = $this->repository->getModel()->select('*');

        $data = $this->paginate($prepare);

        return $this->respond($data);
    }

    /**
     * Show resource
     *
     * @param Request $request
     * @param int $id
     * @return json
     */
    public function show(Request $request, $id)
    {
        $user = $this->test->find($id);
        if ($this->isOwner($request->user(), $user)) {
            return $this->respondForbidden('Unauthorized!');
        }

        return $this->respondSuccess('Success Data!');
    }
}
