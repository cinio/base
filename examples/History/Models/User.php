<?php

namespace Example\History\Models;

use Cinio\Base\Models\Contracts\BypassableContract;
use Cinio\Base\Models\Contracts\Stateable;
use Cinio\Base\Models\Relations\HistoryRelation;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable implements BypassableContract, Stateable
{
    use HasRolesAndAbilities, HistoryRelation;

    /**
     * Can by bypass for ownership rules
     * @return boolean
     */
    public function bypass()
    {
        return $this->isA('sysadmin');
    }

    /**
     * Get this model foreign key to history model
     * @return string
     */
    public function getForeignKey()
    {
        return 'user_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return UserStatusHistory::class;
    }
}
