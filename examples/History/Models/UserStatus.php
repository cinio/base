<?php

namespace Example\History\Models;

use Illuminate\Database\Eloquent\Model;
use Cinio\Base\Models\Contracts\Stateable;
use Cinio\Base\Models\Concerns\HasDropDown;
use Cinio\Base\Models\Scopes\HasActiveScope;

class UserStatus extends Model implements Stateable
{
    use HasActiveScope, HasDropDown;

    /**
     * Get the state id
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->{$this->getEntityKey()};
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'user_status_id';
    }

    /**
     * Get the entity key
     * @return string
     */
    public function getEntityKey()
    {
        return 'id';
    }
}
