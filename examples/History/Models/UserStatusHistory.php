<?php

namespace Example\History\Models;

use Illuminate\Database\Eloquent\Model;
use Cinio\Base\Models\Scopes\HasCurrentScope;

class UserStatusHistory extends Model
{
    use HasCurrentScope;

    /**
     * This model's relation to user status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(UserStatus::class, 'user_status_id');
    }
}
