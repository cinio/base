<?php

namespace Modules\User\Repositories;

use Example\History\Contracts\UserStatusContract;
use Example\History\Models\UserStatus;
use Example\History\Models\UserStatusHistory;
use Cinio\Base\Repositories\Concerns\HasActive;
use Cinio\Base\Repositories\Concerns\HasCrud;
use Cinio\Base\Repositories\Concerns\HasHistory;
use Cinio\Base\Repositories\Concerns\HasSlug;
use Cinio\Base\Repositories\Concerns\HasSoftDelete;
use Cinio\Base\Repositories\Repository;

class UserStatusRepository extends Repository implements UserStatusContract
{
    use HasCrud, HasSlug, HasHistory, HasSoftDelete, HasActive;

    /**
     * Constructor
     *
     * @param UserStatus $model
     * @param UserStatusHistory $history
     */
    public function __construct(UserStatus $model, UserStatusHistory $history)
    {
        $this->histroyModel = $history;
        parent::__construct($model);
    }
}
