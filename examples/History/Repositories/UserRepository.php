<?php

namespace Example\History\Repositories;

use Cinio\Base\Repositories\Concerns\HasCrud;
use Cinio\Base\Repositories\Concerns\HasSlug;
use Example\History\Contracts\UserContract;
use Example\History\Models\User;
use Cinio\Base\Repositories\Concerns\HasSoftDelete;
use Cinio\Base\Repositories\Repository;

class UserRepository extends Repository implements UserContract
{
    use HasCrud, HasSlug, HasSoftDelete;

    /**
     * Class constructor.
     *
     * @param User  $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
