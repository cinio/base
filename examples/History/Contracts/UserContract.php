<?php

namespace Example\History\Contracts;

use Cinio\Base\Repositories\Contracts\CrudContract;
use Cinio\Base\Repositories\Contracts\SlugContract;
use Cinio\Base\Repositories\Contracts\SoftDeleteable;

interface UserContract extends CrudContract, SlugContract, SoftDeleteable
{
}
