<?php

namespace Example\History\Contracts;

use Cinio\Base\Repositories\Contracts\ActiveContract;
use Cinio\Base\Repositories\Contracts\CrudContract;
use Cinio\Base\Repositories\Contracts\HistoryableContract;
use Cinio\Base\Repositories\Contracts\SlugContract;
use Cinio\Base\Repositories\Contracts\SoftDeleteable;

interface UserStatusContract extends CrudContract, SlugContract, HistoryableContract, SoftDeleteable, ActiveContract
{
}
