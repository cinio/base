<?php

namespace Example\Repository;

use Cinio\Base\Repositories\Concerns\HasCrud;
use Cinio\Base\Repositories\Repository;

class TestRepository extends Repository implements TestContract
{
    use HasCrud;

    /**
     * Class constructor
     * @param Container $app
     * @param Model $model
     */
    public function __construct(Test $model)
    {
        parent::__construct($model);
    }
}
