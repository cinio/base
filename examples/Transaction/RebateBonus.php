<?php

namespace Example\Transaction;

class RebateBonus extends Rebate
{
    /**
     * This is a code snippet from rebate payout for Sacotech
     *
     */
    public function payoutCredit(Carbon $date)
    {
        $startDate     = ($date->copy())->startOfDay();
        $endDate       = ($date->copy())->endOfDay();
        $dateString    = $date->copy()->format('Y-m-d H:i:s');
        $rebatePayouts = RebatePayout::whereBetween('created_at', [$startDate, $endDate])
                            ->doesntHave('payout')
                            ->get();

        $rebatePayoutCredits = [];
        $payoutCredits       = [];
        $generator           = new SimpleGenerator();
        $usdCreditId         = $this->validateCreditType('usd_credit');
        $rebateTxnTypeId     = $this->validateTransactionType('rebate');
        $bankAccounts        = $this->bankAccounts
                                ->all([], 0, ['id', 'reference_id'])
                                ->pluck('id', 'reference_id');
        $txnCodes = [];
        foreach ($rebatePayouts as $rebatePayout) {
            if ($rebatePayout->amount > 0) {
                do {
                    $transactionCode = $generator->generate('SCT');
                } while (isset($txnCodes[$transactionCode]));

                $txnCodes[$transactionCode] = $transactionCode;

                $payoutCredits[] = [
                    'created_at'               => $dateString,
                    'updated_at'               => $dateString,
                    'transaction_date'         => $dateString,
                    'transaction_code'         => $transactionCode,
                    'bank_transaction_type_id' => $rebateTxnTypeId,
                    'bank_credit_type_id'      => $usdCreditId,
                    'bank_account_id'          => $bankAccounts[$rebatePayout->user_id],
                    'done_by'                  => $bankAccounts[$rebatePayout->user_id],
                    'credit'                   => $rebatePayout->amount,
                ];

                /**
                 * This part is where all the transaction codes from credit system
                 * being collated to be saved in database.
                 */
                $rebatePayoutCredits[] = [
                        'rebate_payout_id'    => $rebatePayout->id,
                        'bank_credit_type_id' => $usdCreditId,
                        'percentage'          => 1.0,
                        'amount'              => $rebatePayout->amount,
                        'transaction_code'    => $transactionCode,
                        'created_at'          => $dateString,
                        'updated_at'          => $dateString,
                    ];
            }
        }

        foreach (array_chunk($payoutCredits, $this->insertLimit) as $payoutCredit) {
            BankAccountStatement::insert($payoutCredit);
        }

        foreach (array_chunk($rebatePayoutCredits, $this->insertLimit) as $rebatePayoutCredit) {
            RebatePayoutCredit::insert($rebatePayoutCredit);
        }
    }
}
