-- Create syntax for TABLE 'rebate_payout_credits'
CREATE TABLE `rebate_payout_credits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `rebate_payout_id` int(10) unsigned NOT NULL,
  `bank_credit_type_id` int(10) unsigned NOT NULL,
  `percentage` decimal(12,3) NOT NULL DEFAULT '0.000',
  `amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `transaction_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rebate_payout_credits_rebate_payout_id_index` (`rebate_payout_id`),
  KEY `rebate_payout_credits_bank_credit_type_id_index` (`bank_credit_type_id`),
  KEY `rebate_payout_credits_transaction_code_index` (`transaction_code`),
  KEY `rebate_payout_credits_created_at_index` (`created_at`),
  CONSTRAINT `rebate_payout_credits_bank_credit_type_id_foreign` FOREIGN KEY (`bank_credit_type_id`) REFERENCES `bank_credit_types` (`id`),
  CONSTRAINT `rebate_payout_credits_rebate_payout_id_foreign` FOREIGN KEY (`rebate_payout_id`) REFERENCES `rebate_payouts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153568 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Create syntax for TABLE 'rebate_payouts'
CREATE TABLE `rebate_payouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_investment_id` int(10) unsigned DEFAULT NULL,
  `investment_percentage` decimal(12,6) NOT NULL DEFAULT '0.000000',
  `product_profit` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `profit_tax` decimal(12,6) NOT NULL DEFAULT '0.000000',
  `amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `computed_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `deducted_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`),
  KEY `rebate_payouts_user_id_index` (`user_id`),
  KEY `rebate_payouts_product_id_index` (`product_id`),
  KEY `rebate_payouts_product_investment_id_index` (`product_investment_id`),
  KEY `rebate_payouts_created_at_index` (`created_at`),
  KEY `rebate_payouts_is_special_index` (`is_special`),
  KEY `rebate_payouts_deducted_amount_index` (`deducted_amount`),
  CONSTRAINT `rebate_payouts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `rebate_payouts_product_investment_id_foreign` FOREIGN KEY (`product_investment_id`) REFERENCES `product_investments` (`id`),
  CONSTRAINT `rebate_payouts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153568 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;