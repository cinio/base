<?php

namespace Example\Adhoc;

use Example\Repository\TestRepository;
use Illuminate\Support\ServiceProvider;

class AdhocProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $repository = resolve(TestRepository::class);
        // Adhoc attribute
        $repository->greeting = 'Hello World!';

        // Adhoc function
        TestRepository::macro('sayHello', function () {
            echo $this->greeting;
        });

        $repository->sayHello();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
