# Base

This is a collection of reusable Traits, Contracts, Rules, Helper Classes and Set of Standards/Patterns that every project needs to follow.

## Install

In your composer.json add this
```json
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:cinio/base.git"
        }
    ]
```
Then run this command
<pre>composer require cinio/base</pre>

### Repository

There was a lot of discussion and arguments within the team to either use **[Service](https://medium.com/@davislevine/service-design-patterns-930203c8df37)** or **[Repository](https://medium.com/@pererikbergman/repository-design-pattern-e28c0f3e4a30)**. Each have its own advantage and disadvantages, however the team have decided to use **Repository** for the following reasons

* To have only one standard to follow.
* It's easy to differentiate project specific services from Laravel's services.
* Repository is ideal for small functions like CRUD and can be scaled to service when necessary.
* Repository will always represent one model.
* Can easily decide the name of your repository.

Basically a repository can become a service depending on the situation. Let's put "Pin Purchase" in Affiliate projects as an example. When member purchase, there's a lot of things happening on the backend like deducting credits, adding sponsors in the sponsor tree, activating the pin, etc. In this case we can create multiple repositories for Credits, Sponsor and dependency inject them into Pin Repository.

```php

class PinPurchaseRepository extends Repository implements PinPurchaseContract
{
    use HasCrud, HasSlug;

    protected $pin;
    protected $sponsorTree;
    protected $credit;

    /**
     * Class constructor
     * @param Container $app
     * @param Model $model
     */
    public function __construct(
        PinPurchase $model,
        SponsorTreeContract $sponsorTree,
        BankAccountCreditContract $credit,
        PinContract $pin
    ) {
        parent::__construct($model);
    }
}

```

See [example](https://gitlab.com/cinio/base/tree/master/examples/Repository)

### Dependency Injection

We prefer to use Dependency Injection rather than Facade. This makes our coding cleaner and more flexible. For every repository, it must have its corresponding contract and it must be injected in the Controller rather than the concrete class. See [here](https://laravel.com/docs/5.8/container) how its being done in Laravel.

Below is an example how Contracts are injected in Controllers

```php
class RoleController extends Controller
{
    protected $roleRepository;

    public function __construct(RoleContract $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function store(Store $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only('name', 'title');
            $this->roleRepository->add($data);
            DB::commit();
            return redirect()->route('admin.roles.index')->with('success', __('a_roles.role successfully added'));
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
```

See [example](https://gitlab.com/cinio/base/tree/master/examples/Dependency)

### History 

This pattern is commonly used for status and rank where you need to move the state of a certain subject while keeping its history. To do this is quiet simple

* Add history pivot table for your main table
* Use the HasHistory trait in your class

See [example](https://gitlab.com/cinio/base/tree/master/examples/History)

### Transaction

For credit system, credit and debit always have transaction code. This acts as a reference of your transaction.
This pattern is very helpful for tracking these codes. To do this, do the following

* Add new table with a transaction_code column.
* Store all transaction codes into this table.

See [example](https://gitlab.com/cinio/base/tree/master/examples/Transaction)

### Breakdown

This pattern is useful when you need to breakdown an amount. This is usually used for payouts. To do this, do the following

* Add breakdown table.
* Store all breakdowns into this table.

See [example](https://gitlab.com/cinio/base/tree/master/examples/Breakdown)

## Helper Classes

### Adhoc
If you want your class to be flexible enough in such way that developers can create a new function or attributes into your class without overriding it. These traits come in handy.

* [DynamicAttribute.php](https://gitlab.com/cinio/base/blob/master/src/Adhoc/DynamicAttribute.php) - For adhoc attributes
* [Macroable.php](https://gitlab.com/cinio/base/blob/master/src/Adhoc/Macroable.php) - For adhoc functions

See [example](https://gitlab.com/cinio/base/tree/master/examples/Adhoc)

### Model
These are collections of reusable traits, contracts and helper classes for eloquent model
*  [DropDownCollection.php](https://gitlab.com/cinio/base/blob/master/src/Models/Collections/DropDownCollection.php) - Helper class for converting your model into dropdown data ready.
*  [HasDropDown.php](https://gitlab.com/cinio/base/blob/master/src/Models/Concerns/HasDropDown.php) - Trait needs to be used when converting model to dropdown.
*  [BypassableContract.php](https://gitlab.com/cinio/base/blob/master/src/Models/Contracts/BypassableContract.php) - Needs to be implemented for bypasseable models.
*  [Stateable.php](https://gitlab.com/cinio/base/blob/master/src/Models/Contracts/Stateable.php) - Needs to be implemented when using HasHistory trait.
*  [HistoryRelation.php](https://gitlab.com/cinio/base/blob/master/src/Models/Relations/HistoryRelation.php) - Helper trait for implementing history pattern.
*  [UserRelation.php](https://gitlab.com/cinio/base/blob/master/src/Models/Relations/UserRelation.php) - Helper trait for adding user relation.
*  [HasActiveScope.php](https://gitlab.com/cinio/base/blob/master/src/Models/Scopes/HasActiveScope.php) - Helper trait for getting active records.
* [HasCurrentScope.php](https://gitlab.com/cinio/base/blob/master/src/Models/Scopes/HasCurrentScope.php) - Helper trait for getting current data.

See [example](https://gitlab.com/cinio/base/tree/master/examples/History/Models)

### Pagination
These traits can be used for database and collection pagination.
*  [CollectionPagination.php](https://gitlab.com/cinio/base/blob/master/src/Paginations/CollectionPagination.php)
*  [DatabasePagination.php](https://gitlab.com/cinio/base/blob/master/src/Paginations/DatabasePagination.php)

See [examples](https://gitlab.com/cinio/base/blob/master/examples/Dependency/TestController.php)

### Repositories
These are collection of useful Traits, Contracts and Classes for scaffolding your repository class.

* [Repository.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Repository.php) - All repositories must extend this abstract class.
* [HasActive.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Concerns/HasActive.php) - Trait for getting active or inactive records.
* [HasCrud.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Concerns/HasCrud.php) - Trait for Create, Update, Edit, Delete.
* [HasHistory.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Concerns/HasHistory.php) - Trait for keeping state of a specific model.
* [HasSlug.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Concerns/HasSlug.php) - Trait for finding records by slug.
* [HasSoftDelete.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Concerns/HasSoftDelete.php) - Trait for getting and restoring soft deleted records.
* [ActiveContract.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Contracts/ActiveContract.php) - Contract for HasActive Trait.
* [CrudContract.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Contracts/CrudContract.php) - Contract for HasCrud Trait.
* [HistoryableContract.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Contracts/HistoryableContract.php) - Contract for HasHistory Trait.
* [SlugContract.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Contracts/SlugContract.php) - Contract for HasSlug Trait.
* [SoftDeleteable.php](https://gitlab.com/cinio/base/blob/master/src/Repositories/Contracts/SoftDeleteable.php) - Contract for HasSoftDelete Trait.

See [examples](https://gitlab.com/cinio/base/tree/master/examples/History)

### Response
Useful trait for quick JSON response.

* [ApiResponse.php](https://gitlab.com/cinio/base/blob/master/src/Response/ApiResponse.php)

See [examples](https://gitlab.com/cinio/base/blob/master/examples/Dependency/TestController.php)

### Rules
Collection of useful rules.
*  [CheckOwnership.php](https://gitlab.com/cinio/base/blob/master/src/Rules/CheckOwnership.php)

See [examples](https://gitlab.com/cinio/base/blob/master/examples/Dependency/TestController.php)